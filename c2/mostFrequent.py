from sys import stdin, stdout

if __name__ == '__main__':
# The algorith implemented is O(n), with the lexicographical sorting
# this algorith becames teta(n), would also be possible to
# implement one using a heap in the "sorting" process, this alg
# would be faster(i guess), but still O(n), but harder to implement
# the lexicographical sorting and less understandable.
    r = stdin
    n = int(r.readline())
    cTerms = {}
    for i in xrange(n):
        term = r.readline()
        if term in cTerms:
            cTerms[term] += 1
        else:
            cTerms[term] = 1
    k = int(r.readline())

    greaterVal = 0
    for val in cTerms.values():
        if (val > greaterVal):
            greaterVal = val

    index = [[]]*greaterVal

    for x in cTerms.keys():
        index[cTerms[x]-1] = index[cTerms[x]-1] + [x]
        index[cTerms[x]-1].sort()
        # When we flat the list and read from the end the first terms to print should be the last
        index[cTerms[x]-1].reverse()
        #index[cTerms[x]-1] = sorted(index[cTerms[x]-1])

    # Flatten the list
    index = sum(index, [])
    for i in xrange(k):
        stdout.write(index.pop())
    
