from sys import stdin, stdout

if __name__ == '__main__':
    r = stdin
    n = int(r.readline())
    elements = []
    acc = 1
    zero = False
    zeroPlus = False
    for i in xrange(n):
        e = int(r.readline())
        if (e == 0):
            if zero:
                zeroPlus = True
            zero = True
            elements = elements + [e]
        else:
            elements = elements + [e]
            acc = acc * e
    
    if(zeroPlus):
        for i in xrange(n):
            print 0
    elif(zero):
        for el in elements:
            if(el == 0):
                print acc
            else:
                print 0
    else:
        for el in elements:
            print (acc/el)
