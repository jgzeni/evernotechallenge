from sys import stdin, stdout

# The class that represents the buffer
# My goal was to make the code efficent, but also understandable
class CircularBuffer:
    def __init__(self, size):
        self.size = size
        self.elements = []

    # The list func is always O(n), because needs to go througt the list.
    def list_elements(self):
        for el in self.elements:
            stdout.write(el)

    # This was my great doubt
    # Because just acess a element is always constant(O(1)), but i
    # found that the append time is amortized constant, althought
    # the to time been O(n*n). But thinking that we would not have
    # a lot of memory alocation i guess will efficient.
    def append(self, appendElements):
        n = len(self.elements) + len(appendElements) 
        if (n >= self.size):
            del self.elements[0:(n - self.size)]
        self.elements = self.elements + appendElements

    # The time is the same as search a element.
    # But since i delete by index and always by the begin the
    # time will tend to be constant(O(1))
    def remove(self, n):
        del self.elements[0:n]

# This basically process the file
if __name__ == '__main__':
    r = stdin
    i = r.readline()
    cBuff = CircularBuffer(int(i))
    while(True):
        i = r.readline()
        if (i[0] == "R"):
            n = int(i[2:])
            cBuff.remove(n)
        elif (i[0] == "A"):
            n = int(i[2:])
            for i in xrange(n):
                i = r.readline()
                cBuff.append([i])
        elif (i[0] == "L"):
            cBuff.list_elements()
        elif (i[0] == "Q"):
            break
